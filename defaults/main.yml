---
##### App Install Toggles #####
install_bitwarden:              false
install_vscode:                 false
install_onionshare:             false
install_vlc:                    false
install_freetube:               false
install_audacity:               false
install_simplescreenrecorder:   false
install_kdenlive:               false
install_filezilla:              false
install_keepass2:               false
install_keepassxc:              false
install_gitg:                   false
install_firefox:                false
install_tor_browser:            false
install_signal:                 false
install_discord:                false
install_slack:                  false
install_burp_suite:             false
install_anki:                   false
install_keybase:                false
install_rustdesk:               false
install_anydesk:                false
install_redshift:               false
install_steam:                  false
install_ungoogled_chromium:     false
install_zulucrypt_gui:          false
install_mullvad:                false
install_zoom:                   false
install_virtualbox:             false
install_sqlite_browser:         false
install_baobab:                 false
install_rclone_browser:         false
install_appimage_launcher:      false
install_gpg_frontend:           false
install_qbittorrent:            false
install_musescore:              false
install_owasp_zap:              false
install_jamulus:                false
install_qjackctl:               false
install_gifcurry_prereqs:       false
install_gifcurry:               false
install_synaptic:               false
install_uvr:                    false
install_obs:                    false
install_cameractrls:            false
install_helvum:                 false

##### General Config #####
autostart_dir:                  "{{ ansible_user_dir }}/.config/autostart" # place .desktop files in here to auto-start them

##### Installer URLs #####
steam_deb_install_url:          https://cdn.fastly.steamstatic.com/client/installer/steam.deb
qredshift_zip_install_url:      https://cinnamon-spices.linuxmint.com/files/applets/qredshift@quintao.zip
burp_installer_url:             https://portswigger-cdn.net/burp/releases/download?product=community&version=2022.12.7&type=Linux
rustdesk_installer_deb_url:     https://github.com/rustdesk/rustdesk/releases/download/1.2.1/rustdesk-1.2.1-x86_64.deb
mullvad_install_deb_url:        https://mullvad.net/download/app/deb/latest/
zoom_install_deb_url:           https://zoom.us/client/latest/zoom_amd64.deb
keybase_install_deb_url:        https://prerelease.keybase.io/keybase_amd64.deb
# jamulus_install_deb_url:        https://github.com/jamulussoftware/jamulus/releases/download/r3_9_1/jamulus_3.9.1_ubuntu_amd64.deb # use apt package on Debian/MX
discord_install_deb_url:        https://discord.com/api/download?platform=linux&format=deb
appimage_launcher_deb_url:      https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb

##### AppImage Config #####
appimage_directory:             "{{ ansible_user_dir }}/Applications" # /home/vagrant/Applications

#### GPGFrontend Config #####
gpg_frontend_appimage_url:      https://github.com/saturneric/GpgFrontend/releases/download/v2.0.10/GpgFrontend-2.0.10-linux-x86_64.AppImage
gpg_frontend_appimage:          "{{ appimage_directory }}/GPGFrontend.appimage"

##### Redshift Config #####
uninstall_redshift_gtk:         false
install_qredshift_applet:       false
remove_redshift_conf_file:      false
disable_redshift_service:       false

##### Bitwarden Config #####
bitwarden_appimage:             "{{ appimage_directory }}/Bitwarden.appimage"
bitwarden_icon:                 /usr/share/icons/Bitwarden.png

##### Burp Suite Config #####
burp_installer_checksum:        sha256:1daab223e7a4f45cae75933781d83ad8cb3f715e7e76d5bcf4708af4a9f833bb
burp_installer_script_path:     "{{ ansible_user_dir }}/burp-community-installer.sh"
burp_install_target:            "{{ ansible_user_dir }}/BurpSuiteCommunity"

##### Signal Config #####
signal_signing_key_url:         https://updates.signal.org/desktop/apt/keys.asc
signal_keyring:                 /usr/share/keyrings/signal-desktop-keyring.gpg
signal_apt_repo:                deb [arch=amd64 signed-by={{signal_keyring}}] https://updates.signal.org/desktop/apt xenial main # hard-coded to Xenial to match Signal repo on 1/28

##### Anydesk Config #####
anydesk_signing_key_url:        https://keys.anydesk.com/repos/DEB-GPG-KEY
anydesk_apt_repo:               deb http://deb.anydesk.com all main
anydesk_enable_service:         false
anydesk_service_state:          "stopped"

##### Rustdesk Config #####
rustdesk_enable_service:        false
rustdesk_service_state:         "stopped"

##### Virtualbox Config #####
vbox_user:                      "{{ ansible_user_id }}" # added to vboxusers group for guest additions

### For installing from Virtualbox directly, instead of OS repos
# vbox_signing_key_url:           https://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc
# vbox_keyring:                   /usr/share/keyrings/oracle_vbox_2016.gpg
# vbox_repo:                      deb [arch=amd64 signed-by={{vbox_keyring}}] https://download.virtualbox.org/virtualbox/debian bookworm contrib
# vbox_key_id:                    B9F8D658297AF3EFC18D5CDFA2F683C52980AECF
# vbox_package_name:              virtualbox-7.0


#### Gifcurry Config #####
gifcurry_appimage_url:          https://github.com/lettier/gifcurry/releases/download/6.0.1.0/gifcurry-6.0.1.0-x86_64.AppImage
gifcurry_appimage_path:         "{{ appimage_directory }}/Gifcurry.appimage"

#### MuseScore Config ####
musescore_appimage_url:         https://github.com/musescore/MuseScore/releases/download/v4.2.1/MuseScore-4.2.1.240230938-x86_64.AppImage
musescore_appimage_checksum:    "sha256:07b00a863299499cf6a9f25dab85efb11d086c8947d822018782aec905cbfa35"
musescore_appimage_path:        "{{ appimage_directory }}/Musescore.appimage"

#### UVR Config ####
# uvr_repo_url:                   "https://github.com/Anjok07/ultimatevocalremovergui.git"
# uvr_repo_path:                  "{{ ansible_user_dir }}/UltimateVocalRemover"
# uvr_venv:                       "{{ uvr_repo_path }}/venv"