# Summary
Ansible role for installing a set of general-purpose graphical tools.

There are on/off switches for installing each application (see (defaults/main.yml)[defaults/main.yml])

# Notes
- Depends on Flatpak + Flathub for some software

# Maintenance
- URLs to various installers in (defaults/main.yml)[defaults/main.yml] (`Installer URLs` section). These URLs need updating periodically as new releases of the software become available. The packages are these URLs are not checksummed, they are considered as trusted sources.
- Burp Suite installer is checksummed; update checksum as Portswigger install script is updated

# Supported platforms
Tested against:
- MX Linux KDE version (more thoroughly)
- Linux Mint 21.1 (more throughly)
- Kubuntu 22.04 (less thoroughly)
- Kali rolling (less thoroughly)

Should work with Ubuntu/Debian-based graphical environments that have packages for the tools listed in (tasks/main.yml)[tasks/main.yml]
